from setuptools import setup

setup(name='plaza-xmpp-service',
      version='0.1',
      description='Plaza service to use XMPP bots.',
      author='kenkeiras',
      author_email='kenkeiras@codigoparallevar.com',
      license='Apache License 2.0',
      packages=['plaza_xmpp_service'],
      scripts=['bin/plaza-xmpp-service'],
      include_package_data=True,
      install_requires = [
          'slixmpp',
          'plaza-service',
      ],
      zip_safe=False)
