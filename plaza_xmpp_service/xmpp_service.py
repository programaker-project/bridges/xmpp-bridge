import asyncio
import logging
import sys
from plaza_service import (
    PlazaService,
    ServiceConfiguration,
    MessageBasedServiceRegistration,
    ServiceBlock,
    BlockArgument,
    BlockType,
)

from plaza_gitlab_service import serializers

BOT_ADDRESS = 'plaza@codigoparallevar.com'


class Registerer(MessageBasedServiceRegistration):
    def __init__(self, *args, **kwargs):
        MessageBasedServiceRegistration.__init__(self, *args, **kwargs)

    def get_call_to_action_text(self):
        return '''To enable service open a conversation with <u>{bot_addr}</u> and type the following:

        <registration_code>'''.format(bot_addr=BOT_ADDRESS)


class XMPPService(PlazaService):
    def __init__(self, bot, *args, **kwargs):
        PlazaService.__init__(self, *args, **kwargs)
        self.SUPPORTED_FUNCTIONS = {"get_next_message": self.get_next_message}
        self.bot = bot
        self.bot.on_new_message = self.on_new_message
        self.message_received_event = asyncio.Event()
        self.registerer = Registerer(self)

    def on_new_message(self, msg):
        self.last_message = msg
        self.message_received_event.set()
        self.message_received_event.clear()

    async def get_next_message(self):
        await self.message_received_event.wait()
        return self.last_message['body']

    async def handle_call(self, function_name, arguments, extra_data):
        logging.info("{}({}) # {}".format(
            function_name, ", ".join(arguments), extra_data.user_id))
        return await self.SUPPORTED_FUNCTIONS[function_name](*arguments)

    def handle_configuration(self):
        return ServiceConfiguration(
            service_name="XMPP",
            is_public=False,
            registration=self.registerer,
            blocks=[
                ServiceBlock(
                    id="get_next_message",
                    function_name="get_next_message",
                    message="Get next XMPP message",
                    arguments=[],
                    block_type=BlockType.GETTER,
                    block_result_type=None,  # TODO: change
                )
            ],
        )
